#include "slate/slate.hh"
#include "test.hh"
#include "blas_flops.hh"
#include "lapack_flops.hh"
#include "print_matrix.hh"

#include "scalapack_wrappers.hh"
#include "scalapack_support_routines.hh"
#include "scalapack_copy.hh"

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <utility>

//------------------------------------------------------------------------------
template <typename scalar_t> void test_qdwh_work(Params& params, bool run)
{
    using real_t = blas::real_type<scalar_t>;
    using blas::real;
    using llong = long long;

    // get & mark input values
    int64_t m = params.dim.m();
    int64_t n = params.dim.n();
    int64_t p = params.p();
    int64_t q = params.q();
    int64_t nb = params.nb();
    int64_t ib = params.ib();
    int64_t lookahead = params.lookahead();
    int64_t panel_threads = params.panel_threads();
    bool ref_only = params.ref() == 'o';
    bool ref = params.ref() == 'y' || ref_only;
    bool check = params.check() == 'y' && ! ref_only;
    bool trace = params.trace() == 'y';
    int verbose = params.verbose();
    slate::Origin origin = params.origin();
    slate::Target target = params.target();
    params.matrix.mark();

    // mark non-standard output values
    params.time();
    params.gflops();
    params.ref_time();
    params.ref_gflops();
    params.ortho();

    if (! run)
        return;

    // Local values
    const scalar_t zero = 0;
    const scalar_t one = 1;
    const scalar_t minusone = -1;

    // BLACS/MPI variables
    int ictxt, nprow, npcol, myrow, mycol, info;
    int descA_tst[9], descA_ref[9];
    int descH_tst[9];
    int iam = 0, nprocs = 1;

    // initialize BLACS and ScaLAPACK
    Cblacs_pinfo(&iam, &nprocs);
    slate_assert(p*q <= nprocs);
    Cblacs_get(-1, 0, &ictxt);
    Cblacs_gridinit(&ictxt, "Col", p, q);
    Cblacs_gridinfo(ictxt, &nprow, &npcol, &myrow, &mycol);

    // skip invalid sizes
    if (m <= (p-1)*nb || n <= (q-1)*nb) {
        if (iam == 0) {
            printf("\nskipping: ScaLAPACK requires that all ranks have some rows & columns; "
                   "i.e., m > (p-1)*nb = %lld and n > (q-1)*nb = %lld\n",
                   llong( (p-1)*nb ), llong( (q-1)*nb ));
        }
        return;
    }

    // matrix A, figure out local size, allocate, create descriptor, initialize
    int64_t mlocA = scalapack_numroc(m, nb, myrow, 0, nprow);
    int64_t nlocA = scalapack_numroc(n, nb, mycol, 0, npcol);
    scalapack_descinit(descA_tst, m, n, nb, nb, 0, 0, ictxt, mlocA, &info);
    slate_assert(info == 0);
    int64_t lldA = (int64_t)descA_tst[8];
    std::vector<scalar_t> A_tst(lldA*nlocA);
    //scalapack_pplrnt(&A_tst[0], m, n, nb, nb, myrow, mycol, nprow, npcol, mlocA, iseed + 1);

    int64_t mlocH = scalapack_numroc(n, nb, myrow, 0, nprow);
    int64_t nlocH = scalapack_numroc(n, nb, mycol, 0, npcol);
    scalapack_descinit(descH_tst, n, n, nb, nb, 0, 0, ictxt, mlocH, &info);
    slate_assert(info == 0);
    int64_t lldH = (int64_t)descH_tst[8];
    std::vector<scalar_t> H_tst(lldH*nlocH);
    //scalapack_pplrnt(&H_tst[0], n, n, nb, nb, myrow, mycol, nprow, npcol, mlocH, iseed + 1);
    //scalapack_pplrnt(&A_tst[0], n, n, nb, nb, myrow, mycol, nprow, npcol, mlocH, iseed + 1);

    // matrix QR, for checking result
    slate::Matrix<scalar_t> Id;
    Id = slate::Matrix<scalar_t>(n, n, nb, nprow, npcol, MPI_COMM_WORLD);
    Id.insertLocalTiles();

    slate::Matrix<scalar_t> A;
    slate::Matrix<scalar_t> H;
    if (origin != slate::Origin::ScaLAPACK) {
        // Copy local ScaLAPACK data to GPU or CPU tiles.
        slate::Target origin_target = origin2target(origin);
        A = slate::Matrix<scalar_t>(m, n, nb, nprow, npcol, MPI_COMM_WORLD);
        A.insertLocalTiles(origin_target);
        copy(&A_tst[0], descA_tst, A);
        H = slate::Matrix<scalar_t>(n, n, nb, nprow, npcol, MPI_COMM_WORLD);
        H.insertLocalTiles(origin_target);
        copy(&H_tst[0], descH_tst, H);
    }
    else {
        // create SLATE matrices from the ScaLAPACK layouts
        A = slate::Matrix<scalar_t>::fromScaLAPACK(m, n, &A_tst[0], lldA, nb, nprow, npcol, MPI_COMM_WORLD);
        H = slate::Matrix<scalar_t>::fromScaLAPACK(n, n, &H_tst[0], lldH, nb, nprow, npcol, MPI_COMM_WORLD);
    }

    if (verbose > 1) {
        print_matrix("A", A);
    }

    // if check is required, copy test data and create a descriptor for it
    std::vector<scalar_t> A_ref;
    slate::Matrix<scalar_t> Aref;
    if (check || ref) {
        A_ref = A_tst;
        scalapack_descinit(descA_ref, m, n, nb, nb, 0, 0, ictxt, mlocA, &info);
        slate_assert(info == 0);

        Aref = slate::Matrix<scalar_t>::fromScaLAPACK(
            m, n, &A_ref[0], lldA, nb, nprow, npcol, MPI_COMM_WORLD);
    }

    params.matrix.kind.set_default("svd");
    params.matrix.cond.set_default(1.e16);

    slate::generate_matrix( params.matrix, A);
    copy(A, &A_ref[0], descA_ref);

    double gflop = lapack::Gflop<scalar_t>::geqrf(m, n);

    if (! ref_only) {
        if (trace) slate::trace::Trace::on();
        else slate::trace::Trace::off();

        {
            slate::trace::Block trace_block("MPI_Barrier");
            MPI_Barrier(MPI_COMM_WORLD);
        }
        double time = testsweeper::get_wtime();

        //==================================================
        // Run SLATE test.
        //==================================================
        // A = AH, 
        // A will be written by the orthogonal polar factor
        // H is the symmetric positive semidefinite polar factor
        slate::qdwh(A, H, {
            {slate::Option::Lookahead, lookahead},
            {slate::Option::Target, target},
            {slate::Option::MaxPanelThreads, panel_threads},
            {slate::Option::InnerBlocking, ib}
        });


        {
            slate::trace::Block trace_block("MPI_Barrier");
            MPI_Barrier(MPI_COMM_WORLD);
        }
        double time_tst = testsweeper::get_wtime() - time;
   
        if (trace) slate::trace::Trace::finish();

        // compute and save timing/performance
        params.time() = time_tst;
        params.gflops() = gflop / time_tst;

        if (verbose > 1) {
            print_matrix("U: orthogonal polar factor", A);
            print_matrix("H: symmetric postive semi definite factor", H);
        }
    }

    if (check) {
        //==================================================
        // Test results by checking the orthogonality of U factor
        //
        //      || A'A - I ||_f
        //     ---------------- < tol * epsilon
        //            n
        //
        //==================================================
        auto AT = conj_transpose(A); 
        set(zero, one, Id);
        gemm(one, AT, A, minusone, Id);
        real_t orth = norm(slate::Norm::Fro, Id);
        params.ortho() = orth / sqrt(n);

        //==================================================
        // Test results by checking backwards error
        //
        //      || A'H - Aref ||_f
        //     -------------------    < tol * epsilon
        //          ||Aref||_f
        //
        //==================================================
        real_t normA = norm(slate::Norm::Fro, Aref);
        gemm(one, A, H, minusone, Aref);
        real_t berr = norm(slate::Norm::Fro, Aref);
        params.error() = berr / normA;

        real_t tol = params.tol() * 0.5 * std::numeric_limits<real_t>::epsilon();
        params.okay() = ((params.error() <= tol) && (params.ortho() <= tol));


    }
}

// -----------------------------------------------------------------------------
void test_qdwh(Params& params, bool run)
{
    switch (params.datatype()) {
        case testsweeper::DataType::Integer:
            throw std::exception();
            break;

        case testsweeper::DataType::Single:
            test_qdwh_work<float> (params, run);
            break;

        case testsweeper::DataType::Double:
            test_qdwh_work<double> (params, run);
            break;

        case testsweeper::DataType::SingleComplex:
            test_qdwh_work<std::complex<float>> (params, run);
            break;

        case testsweeper::DataType::DoubleComplex:
            test_qdwh_work<std::complex<double>> (params, run);
            break;
    }
}
