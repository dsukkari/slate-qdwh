
//------------------------------------------------------------------------------
// Copyright (c) 2017, University of Tennessee
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the University of Tennessee nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL UNIVERSITY OF TENNESSEE BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//------------------------------------------------------------------------------
// This research was supported by the Exascale Computing Project (17-SC-20-SC),
// a collaborative effort of two U.S. Department of Energy organizations (Office
// of Science and the National Nuclear Security Administration) responsible for
// the planning and preparation of a capable exascale ecosystem, including
// software, applications, hardware, advanced system engineering and early
// testbed platforms, in support of the nation's exascale computing imperative.
//------------------------------------------------------------------------------
// For assistance with SLATE, email <slate-user@icl.utk.edu>.
// You can also join the "SLATE User" Google group by going to
// https://groups.google.com/a/icl.utk.edu/forum/#!forum/slate-user,
// signing in with your Google credentials, and then clicking "Join group".
//------------------------------------------------------------------------------

#include "slate/slate.hh"
#include "aux/Debug.hh"
#include "slate/Matrix.hh"
#include "internal/internal.hh"

#include "../testsweeper/testsweeper.hh"

namespace slate {


template <typename scalar_t>
void qdwh(Matrix<scalar_t>& A,
          Matrix<scalar_t>& H, // this matrix will be hermition
          const std::map<Option, Value>& opts)
{
    using real_t = blas::real_type<scalar_t>;
    using blas::real;
    //const Layout layout = Layout::ColMajor;

    int64_t mt = A.mt();
    int64_t nt = A.nt();

    int64_t m    = A.m();
    int64_t n    = A.n();
    int64_t nb   = A.tileMb(0);
    int64_t m2   = m + n;

    int64_t nprow    = A.internalP();
    int64_t npcol    = A.internalQ(); 

    bool optqr = 1;
    int itconv, it;
    //int itqr = 0, itpo = 0;

    real_t eps  = 0.5 * std::numeric_limits<real_t>::epsilon();
    real_t tol1 = 5. * eps;
    real_t tol3 = pow(tol1, 1./3.);

    double L2, sqd, dd, a1, a, b, c;
    real_t Li, Liconv;
    real_t conv = 100.;
    scalar_t alpha, beta; 
    scalar_t zero = 0.0, one = 1.0, minusone = -1.0; 


    real_t normA;
    real_t norminvR;

    int64_t mt2 = 2*mt - 1;

    // allocate m2*n work space required for the qr-based iterations
    // this allocation can be avoided if we change the qr iterations to
    // work on two matrices on top of each other

    // if doing QR([A;Id])
    slate::Matrix<scalar_t> W(m2, n, nb, nprow, npcol, MPI_COMM_WORLD);              
    W.insertLocalTiles(); 
    auto W1 = W.sub(0, mt-1, 0, nt-1); // First mxn block of W
    auto W2 = W.sub(mt, mt2, 0, nt-1);  // Second nxn block of W

    slate::Matrix<scalar_t> Q(m2, n, nb, nprow, npcol, MPI_COMM_WORLD);              
    Q.insertLocalTiles(); 

    // if doing QR([A]) QR([A;Id]) 
    //slate::Matrix<scalar_t> W1(m, n, nb, nprow, npcol, MPI_COMM_WORLD);              
    //slate::Matrix<scalar_t> W2(m, n, nb, nprow, npcol, MPI_COMM_WORLD);              
    //W1.insertLocalTiles(); 
    //W2.insertLocalTiles(); 

    slate::TriangularFactors<scalar_t> T1;
    auto Q1 = Q.sub(0, mt-1, 0, nt-1); // First mxn block of W
    auto Q2 = Q.sub(mt, mt2, 0, nt-1);  // Second nxn block of W

    // backup A in H
    copy(A, H);

    // two norm estimation (largest singular value of A)
    real_t norm_est = normest(slate::Norm::One, A);
    alpha = 1.0;

    // scale the original matrix A to form A0 of the iterative loop
    geadd(alpha/(scalar_t)norm_est, H, zero, A, opts);

    // Calculate Li: reciprocal of condition number estimation
    // Either 1) use LU followed by gecon
    // Or     2) QR followed by trtri
    // If used the QR, use the Q factor in the first QR-based iteration
    normA = norm(slate::Norm::One, A);
    if (optqr) {
        // Estimate the condition number using QR */
        // This Q factor can be used in the first QR-based iteration
        copy(A, W1);
        slate::geqrf(W1, T1, opts);
        auto R = slate::TriangularMatrix<scalar_t>( 
                   slate::Uplo::Upper, slate::Diag::NonUnit, W1 );  
        auto Rh = slate::HermitianMatrix<scalar_t>( 
                   slate::Uplo::Upper, W1 );  
        slate::trtri(R, opts);
        //tzset(scalar_t(0.0), L);
        norminvR = norm(slate::Norm::One, Rh);
        Li = (1.0 / norminvR) / normA ;
        Li = norm_est / 1.1 * Li;
        //*flops += FLOPS_DGEQRF( M, N )
        //       + FLOPS_DTRTRI( N );
    }
    else {
        // todo 
        // Estimate the condition number using LU */
    }

    itconv = 0; Liconv = Li;
    while (itconv == 0 || fabs(1-Liconv) > tol1) {
	// To find the minimum number of iterations to converge. 
        // itconv = number of iterations needed until |Li - 1| < tol1 
	// This should have converged in less than 50 iterations
	if (itconv > 100) {
	    exit(-1);
	    break;
	}
	itconv++;

	L2  = double (Liconv * Liconv);
	dd  = pow( 4.0 * (1.0 - L2 ) / (L2 * L2), 1.0/3.0 );
	sqd = sqrt(1.0 + dd);
	a1  = sqd + sqrt( 8.0 - 4.0 * dd + 8.0 * (2.0 - L2) / (L2 * sqd) ) / 2.0;
	a   = real(a1);
	b   = (a - 1.0) * (a - 1.0) / 4.0;
	c   = a + b - 1.0;
	// Update Liconv
	Liconv  = Liconv * real_t((a + b * L2) / (1.0 + c * L2));
    }

    it = 0;
    while (conv > tol3 || it < itconv) {
       	// This should have converged in less than 50 iterations
	if (it > 100) {
	    exit(-1);
	    break;
	}
	it++;

	// Compute parameters L,a,b,c
	L2  = double (Li * Li);
	dd  = pow( 4.0 * (1.0 - L2 ) / (L2 * L2), 1.0/3.0 );
	sqd = sqrt(1.0 + dd);
	a1  = sqd + sqrt( 8.0 - 4.0 * dd + 8.0 * (2.0 - L2) / (L2 * sqd) ) / 2.0;
	a   = real(a1);
	b   = (a - 1.0) * (a - 1.0) / 4.0;
	c   = a + b - 1.0;
	// Update Li
	Li  = Li * real_t ((a + b * L2) / (1.0 + c * L2));

        if (real(c) > 100.) {
            //int doqr = !optqr || it > 1;

	    // Generate the matrix B = [ B1 ] = [ sqrt(c) * U ]
	    //                         [ B2 ] = [ Id          ]
            alpha = scalar_t(sqrt(c));
            set(zero, one, W2);
  
            // todo: have a customized splitted QR
	    //if( doqr ) {
            //    geadd(alpha, A, zero, W1);
            //    geqrf_qdwh_full(W, T1, opts); 
            //}
	    //else {
            //    geadd(alpha, W1, zero, W1);
            //    geqrf_qdwh_full2(W, T1, opts); 
            //}

	    // Factorize B = QR, and generate the associated Q
            geadd(alpha, A, zero, W1, opts);
            //geqrf(W, T1, opts); // naive impl 
            geqrf_qdwh_full(W, T1, opts); 

            set(zero, one, Q1);
            set(zero, zero, Q2);

            //unmqr(slate::Side::Left, slate::Op::NoTrans, W, T1, Q, opts); //naive impl
            unmqr_qdwh_full(slate::Side::Left, slate::Op::NoTrans, W, T1, Q, opts);

	    // A = ( (a-b/c)/sqrt(c) ) * Q1 * Q2' + (b/c) * A
            auto Q2T = conj_transpose(Q2);
            alpha = scalar_t( (a-b/c)/sqrt(c) );
            beta  = scalar_t( b / c );
	    // Copy U into C to check the convergence of QDWH
            if (it >= itconv ){
                copy(A, W1);
            }
            gemm(alpha, Q1, Q2T, beta, A, opts);

	    /* Main flops used in this step */
	    //flops_dgeqrf = FLOPS_DGEQRF( 2*m, n );
	    //flops_dorgqr = FLOPS_DORGQR( 2*m, n, n );
	    //flops_dgemm  = FLOPS_DGEMM( m, n, n );
	    //flops += flops_dgeqrf + flops_dorgqr + flops_dgemm;

            //itqr += 1;
	    //facto = 0;
	}
	else {
	    // Copy A into H to check the convergence of QDWH
            if (it >= itconv) {
                copy(A, W1);
            }

            // Make Q1 into an identity matrix
            set(zero, one, W2);

	    // Compute Q1 = c * A' * A + I
	    ///////////////
            copy(A, Q1);
            auto AT = conj_transpose(Q1);
            gemm(scalar_t(c), AT, A, one, W2, opts);

	    // Solve R x = AT
            auto R = slate::HermitianMatrix<scalar_t>( 
                   slate::Uplo::Upper, W2 );  
            posv(R, AT, opts);

	    // Update A =  (a-b/c) * Q1T' + (b/c) * A
            alpha = (a-b/c); beta = (b/c);
            AT = conj_transpose(AT);
            geadd(scalar_t(alpha), AT, scalar_t(beta), A, opts);

	    // Main flops used in this step
	    //flops_dgemm  = FLOPS_DGEMM( m, n, n );
	    //flops_dpotrf = FLOPS_DPOTRF( m );
	    //flops_dtrsm  = FLOPS_DTRSM( 'L', m, n );
	    //flops += flops_dgemm + flops_dpotrf + 2. * flops_dtrsm;

            //itpo += 1;
	    //facto = 1;
        }

	// Compute the norm of the symmetric matrix U - B1
        conv = 10.0;
        if (it >= itconv) {
            geadd(one, A, minusone, W1, opts);
            conv = norm(slate::Norm::Fro, W1);
        }
    }

    // A = U*H ==> H = U'*A ==> H = 0.5*(H'+H)
    copy(H, W1);
    auto AT = conj_transpose(A);
    gemm(one, AT, W1, zero, H, opts);
}


//------------------------------------------------------------------------------
// Explicit instantiations.
template
void qdwh<float>(
    Matrix<float>& A,
    Matrix<float>& H,
    const std::map<Option, Value>& opts);

template
void qdwh<double>(
    Matrix<double>& A,
    Matrix<double>& H,
    const std::map<Option, Value>& opts);

template
void qdwh< std::complex<float> >(
    Matrix< std::complex<float> >& A,
    Matrix< std::complex<float> >& H,
    const std::map<Option, Value>& opts);

template
void qdwh< std::complex<double> >(
    Matrix< std::complex<double> >& A,
    Matrix< std::complex<double> >& H,
    const std::map<Option, Value>& opts);

} // namespace slate
