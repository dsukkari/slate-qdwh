//------------------------------------------------------------------------------
// Copyright (c) 2017, University of Tennessee
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the University of Tennessee nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL UNIVERSITY OF TENNESSEE BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//------------------------------------------------------------------------------
// This research was supported by the Exascale Computing Project (17-SC-20-SC),
// a collaborative effort of two U.S. Department of Energy organizations (Office
// of Science and the National Nuclear Security Administration) responsible for
// the planning and preparation of a capable exascale ecosystem, including
// software, applications, hardware, advanced system engineering and early
// testbed platforms, in support of the nation's exascale computing imperative.
//------------------------------------------------------------------------------
// For assistance with SLATE, email <slate-user@icl.utk.edu>.
// You can also join the "SLATE User" Google group by going to
// https://groups.google.com/a/icl.utk.edu/forum/#!forum/slate-user,
// signing in with your Google credentials, and then clicking "Join group".
//------------------------------------------------------------------------------

#include "slate/slate.hh"
#include "internal/internal.hh"
#include "internal/internal_util.hh"
#include "slate/internal/mpi.hh"
#include "../test/print_matrix.hh"

#include <list>
#include <tuple>

namespace slate {

// specialization namespace differentiates, e.g.,
// internal::norm from internal::specialization::norm
namespace internal {
namespace specialization {

//------------------------------------------------------------------------------
/// @internal
/// Distributed parallel general matrix norm.
/// Generic implementation for any target.
/// @ingroup norm_specialization
///
template <Target target, typename matrix_type>
blas::real_type<typename matrix_type::value_type>
normest(slate::internal::TargetType<target>,
     Norm in_norm, matrix_type A)
{
    using scalar_t = typename matrix_type::value_type;
    using real_t = blas::real_type<scalar_t>;

    // Undo any transpose, which switches one <=> inf norms.
    if (A.op() == Op::ConjTrans)
        A = conj_transpose(A);
    else if (A.op() == Op::Trans)
        A = transpose(A);

    //---------
    // second norm estimation
    // First: let's compute the x vector such that
    // x_j = sum_i abs( A_{i,j} ), x here is global_sums.data
    if (in_norm == Norm::One) {

        using blas::min; 

        int64_t p = A.internalP();
        int64_t q = A.internalQ();

        int64_t n = A.n();
        int64_t m = A.m();
        int64_t nb = A.tileNb(0);

        scalar_t one = 1.;
        scalar_t zero  = 0.;
        real_t alpha;

        int64_t cnt = 0;
        int64_t maxiter = min( 100, n );

        real_t e  = 0.;
        real_t e0  = 0.;
        real_t normX = 0;
        real_t normAX = 0;
        real_t tol = 1.e-1;

        std::vector<real_t> local_sums(n);

        if (target == Target::Devices)
            A.reserveDeviceWorkspace();

        std::vector<scalar_t> global_sums(n);
        std::vector<scalar_t> W1(n);
        std::vector<scalar_t> W2(n);

        auto XL = slate::Matrix<scalar_t>::fromLAPACK(                           
            n, 1, &global_sums[0], n, nb, 1, p, q, A.mpiComm()); 
        XL.insertLocalTiles();
        auto AX = slate::Matrix<scalar_t>::fromLAPACK(                           
            m, 1, &W1[0], m, nb, 1, p, q, A.mpiComm()); 
        AX.insertLocalTiles();
        auto X = slate::Matrix<scalar_t>::fromLAPACK(                           
            n, 1, &W2[0], n, nb, 1, p, q, A.mpiComm()); 
        X.insertLocalTiles();


        #pragma omp parallel
        #pragma omp master
        {
            internal::norm<target>(in_norm, NormScope::Matrix, std::move(A), local_sums.data());
        }

        #pragma omp critical(slate_mpi)
        {
            trace::Block trace_block("MPI_Allreduce");
            slate_mpi_call(
                MPI_Allreduce(local_sums.data(), global_sums.data(),
                              n, mpi_type<real_t>::value,
                              MPI_SUM, A.mpiComm()));
        }

        A.clearWorkspace();
        // global_sums.data() will have the sum of each column. 
        // First step is done

        // Second: compute the ||x||_Fro
        //e = lapack::lange(
        //    Norm::Fro, 1, n, 
        //    global_sums.data(), 1); 
        e  = slate::norm(Norm::Fro, XL);
        if (e == 0.) {
            return 0.;
        }
        else {
            normX = e;
        }

        // Third: start the while-loop X = X / ||X||
        while ( (cnt < maxiter) &&
            (fabs((e - e0)) > (tol * (e))) )
        {
            e0 = e;

            // Scale X = X / ||X|| 
            alpha = 1.0/normX;
            geadd(scalar_t(alpha), XL, zero, X);

            // Compute Ax = A * sx
            slate::gemmA(one, A, X, zero, AX);

            // Compute x = A' * A * x = A' * Ax
            auto AT = conj_transpose(A);
            slate::gemmA(one, AT, AX, zero, XL);

            // Compute ||X||, ||AX||
            normX  = slate::norm(Norm::Fro, XL);
            normAX = slate::norm(Norm::Fro, AX);
   
            // Uodate e
            e = normX / normAX;
            cnt++;
	} 
        if ( (cnt >= maxiter) &&
            (fabs((e - e0)) > (tol * e)) ) {
        }
        return e;
    }
    else {
        throw std::exception();  // todo: invalid norm
    }
}

} // namespace specialization
} // namespace internal

//------------------------------------------------------------------------------
/// Version with target as template parameter.
/// @ingroup norm_specialization
///
template <Target target, typename matrix_type>
blas::real_type<typename matrix_type::value_type>
normest(Norm norm, matrix_type& A,
     const std::map<Option, Value>& opts)
{
    return internal::specialization::normest(internal::TargetType<target>(),
                                          norm, A);
}

//------------------------------------------------------------------------------
/// Distributed parallel general matrix norm.
///
//------------------------------------------------------------------------------
/// @tparam matrix_type
///     Any SLATE matrix type: Matrix, SymmetricMatrix, HermitianMatrix,
///     TriangularMatrix, etc.
//------------------------------------------------------------------------------
/// @param[in] in_norm
///     Norm to compute:
///     - Norm::Second: second norm estimation of the matrix$
///
/// @param[in] A
///     The matrix A.
///
/// @param[in] opts
///     Additional options, as map of name = value pairs. Possible options:
///     - Option::Target:
///       Implementation to target. Possible values:
///       - HostTask:  OpenMP tasks on CPU host [default].
///       - HostNest:  nested OpenMP parallel for loop on CPU host.
///       - Devices:   batched BLAS on GPU device.
///
/// @ingroup norm
///
template <typename matrix_type>
blas::real_type<typename matrix_type::value_type>
normest(Norm in_norm, matrix_type& A,
     const std::map<Option, Value>& opts)
{
    Target target;
    try {
        target = Target(opts.at(Option::Target).i_);
    }
    catch (std::out_of_range&) {
        target = Target::HostTask;
    }

    switch (target) {
        case Target::Host:
        case Target::HostTask:
            return normest<Target::HostTask>(in_norm, A, opts);
            break;
        case Target::HostBatch:
        case Target::HostNest:
        case Target::Devices:
            break;
    }
    throw std::exception();  // todo: invalid target
}

//------------------------------------------------------------------------------
// Explicit instantiations.
template
float normest(
    Norm in_norm, Matrix<float>& A,
    const std::map<Option, Value>& opts);

template
double normest(
    Norm in_norm, Matrix<double>& A,
    const std::map<Option, Value>& opts);

template
float normest(
    Norm in_norm, Matrix< std::complex<float> >& A,
    const std::map<Option, Value>& opts);

template
double normest(
    Norm in_norm, Matrix< std::complex<double> >& A,
    const std::map<Option, Value>& opts);

} // namespace slate
